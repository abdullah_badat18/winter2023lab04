import java.util.Scanner;
public class ApplianceStore
{ 
	public static void main(String[] args)
 	{
  		Scanner sc = new Scanner(System.in);
  
  		Microwave[] micro = new Microwave[4];
  		for(int i=0; i<micro.length; i++)
  		{
			System.out.println("What is the brand of the microwave? ");
    			String brand = sc.next();
	
			System.out.println("What is the material of the microwave? ");
    			String material = sc.next();

    			System.out.println("What is the temperature level of the microwave? ");
			double temperature = sc.nextDouble();
	
    			micro[i] = new Microwave(brand, material, temperature);
  		}
   
    		micro[0].start();
  		micro[0].quality();
	
		micro[1].heatFood(100);
	
		System.out.println(micro[micro.length-1].getBrand());
    		System.out.println(micro[micro.length-1].getMaterial());
    		System.out.println(micro[micro.length-1].getTemperature());
	
		micro[micro.length-1].setMaterial("platinum");
		micro[micro.length-1].setTemperature(45);
	
		System.out.println(micro[micro.length-1].getBrand());
    		System.out.println(micro[micro.length-1].getMaterial());
    		System.out.println(micro[micro.length-1].getTemperature());
 	}
}




