public class Microwave
{
  	private String brand;
  	private String material;
  	private double temperature; 

	//Constructor
   	public Microwave(String brand, String material, double temperature)
   	{
		this.brand = "maytag";
	   	this.material = "steel";
	   	this.temperature = 99;
   	}

  	public void start()
  	{
    		if(this.brand.equals("maytag"))
    		{
      			System.out.println("WOW! the heat level of this microwave is tremendous.");
    		}
    		else
    		{
      			System.out.println("You should've got another better brand!");
    		}
  	} 

  	public void quality() 
  	{
    		if(this.material.equals("steel"))
    		{ 
     			System.out.println("Best quality! It will last years!");
    		}
    		else
    		{
      			System.out.println("Don't be shocked if you see scratches on it!");
    		}
  	}

   	public void heatFood(double newTemperature)
   	{
	   
	   	if(newTemperature >= 100)
	   	{
			System.out.println("Wow perfect heat temperature.");
			this.temperature = newTemperature;
	   	}
   	}

  	//Getter Methods 
   	public String getBrand()
   	{
		return this.brand;
   	}
   
   	public String getMaterial()
   	{
	   	return this.material;
   	}
   
   	public double getTemperature()
   	{
		return this.temperature;
   	}
   
  	//Setter Methods
   	public void setMaterial(String newMaterial)
   	{
		this.material = newMaterial;
   	}
   	public void setTemperature(double newTemperature)
   	{
		this.temperature = newTemperature;  
   	}
}
